#!/usr/bin/env python

import os
import sys

for directory in os.walk("src"):
    for file in directory[2]:
        if file.endswith(".cpp") or file.endswith(".hpp"):
            path = directory[0] + "/" + file
            print(path)
            os.system("clang-format -i " + path)