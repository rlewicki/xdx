#include <vulkan/vulkan.h>

#define SDL_MAIN_HANDLED

#include "renderer/vulkan/Vulkan.hpp"
#include "window/SdlWindow.hpp"

int main(int argc, char** argv) {
  SdlWindow window;
  window.init("xdx", 1280, 720);
  Vulkan vulkan;
  vulkan.init();
  while (window.update()) {
  }
  return 0;
}