#include "SdlWindow.hpp"

void SdlWindow::init(const char* title, uint32_t width, uint32_t height) {
  m_sdlWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED,
                                 SDL_WINDOWPOS_CENTERED, width, height, 0);
}

bool SdlWindow::update() {
  SDL_Event ev;
  while (SDL_PollEvent(&ev)) {
    auto code = ev.key.keysym.scancode;
    if (code == SDL_SCANCODE_ESCAPE || ev.type == SDL_QUIT) {
      return false;
    }
  }
  return true;
}
