#ifndef XDX_SDLWINDOW_HPP
#define XDX_SDLWINDOW_HPP

#include <string>

#include <SDL.h>

class SdlWindow {
 public:
  void init(const char* title, uint32_t width, uint32_t height);
  bool update();

 private:
  SDL_Window* m_sdlWindow;
};

#endif  // XDX_SDLWINDOW_HPP
