#ifndef XDX_VULKAN_HPP
#define XDX_VULKAN_HPP

#include <vulkan/vulkan.h>

#include <array>
#include <vector>

class Vulkan {
 public:
  void init();
  void release();

 private:
  void createInstance();
  void createPhysicalDevice();
  void createQueues();

  void validateLayers();
  void validateExtensions();
  std::vector<std::string> queryAvailableLayerNames();
  std::vector<std::string> queryAvailableExtensions();
  std::vector<std::string> queryExtensionsForLayer(const char* layerName);

  const std::vector<const char*> m_layerNames = {
      "VK_LAYER_LUNARG_standard_validation",
  };

  const std::vector<const char*> m_extensionNames = {
      "VK_KHR_surface",
#ifdef WIN32
      "VK_KHR_win32_surface",
#endif
  };

  VkInstance m_instance = VK_NULL_HANDLE;
  VkPhysicalDevice m_physicalDevice = VK_NULL_HANDLE;
};

#endif  // XDX_VULKAN_HPP
