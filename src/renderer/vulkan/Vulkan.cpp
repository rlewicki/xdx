#include "Vulkan.hpp"

#include <algorithm>
#include <cassert>
#include <sstream>

#include "../../Utils.hpp"

void Vulkan::init() {
  validateLayers();
  validateExtensions();
  createInstance();
  createPhysicalDevice();
  createQueues();
  XDX_MSG("Vulkan initialized correctly!");
}

void Vulkan::createInstance() {
  VkApplicationInfo appInfo = {};
  appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pApplicationName = "XDX_TEST";
  appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
  appInfo.pNext = nullptr;
  appInfo.pEngineName = "XDX_TEST";
  appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
  appInfo.apiVersion = VK_API_VERSION_1_1;

  VkInstanceCreateInfo instanceInfo = {};
  instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instanceInfo.flags = 0;
  instanceInfo.pNext = nullptr;
  instanceInfo.pApplicationInfo = &appInfo;
  instanceInfo.enabledLayerCount = m_layerNames.size();
  instanceInfo.ppEnabledLayerNames = m_layerNames.data();
  instanceInfo.enabledExtensionCount = m_extensionNames.size();
  instanceInfo.ppEnabledExtensionNames = m_extensionNames.data();

  VkResult result = vkCreateInstance(&instanceInfo, nullptr, &m_instance);
  assert(result == VK_SUCCESS);
}

void Vulkan::createPhysicalDevice() {
  uint32_t physicalDeviceCount;
  VkResult result =
      vkEnumeratePhysicalDevices(m_instance, &physicalDeviceCount, nullptr);
  assert(result == VK_SUCCESS);
  std::vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
  result = vkEnumeratePhysicalDevices(m_instance, &physicalDeviceCount,
                                      physicalDevices.data());
  assert(result == VK_SUCCESS);
  m_physicalDevice = physicalDevices[0];
  // TODO
  // Create function that will print information about selected physical device
}

void Vulkan::createQueues() {
  uint32_t queueFamilyPropertyCount;
  vkGetPhysicalDeviceQueueFamilyProperties(m_physicalDevice,
                                           &queueFamilyPropertyCount, nullptr);
  std::vector<VkQueueFamilyProperties> queueFamilyProperties(
      queueFamilyPropertyCount);
  vkGetPhysicalDeviceQueueFamilyProperties(m_physicalDevice,
                                           &queueFamilyPropertyCount,
                                           queueFamilyProperties.data());

  uint32_t graphicsBitFamiliesCount = 0;
  uint32_t transferBitFamiliesCount = 0;
  for (const auto& queueFamily : queueFamilyProperties) {
    if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
      graphicsBitFamiliesCount++;
    }

    if (queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT) {
      transferBitFamiliesCount++;
    }
  }
  std::stringstream ss;
  ss << "Queue families with graphics bit on selected physical device: "
     << graphicsBitFamiliesCount;
  XDX_MSG(ss.str().c_str());
  ss.str("");
  ss << "Queue families with transfer bit on selected physical device: "
     << transferBitFamiliesCount;
  XDX_MSG(ss.str().c_str());
}

std::vector<std::string> Vulkan::queryAvailableLayerNames() {
  uint32_t layerCount;
  VkResult result = vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
  assert(result == VK_SUCCESS);
  std::vector<VkLayerProperties> layerProperties(layerCount);
  result =
      vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data());
  assert(result == VK_SUCCESS);

  std::vector<std::string> layerNames(layerCount);

  for (size_t i = 0; i < layerCount; i++) {
    layerNames[i] = layerProperties[i].layerName;
  }

  return layerNames;
}

std::vector<std::string> Vulkan::queryAvailableExtensions() {
  return queryExtensionsForLayer(nullptr);
}

std::vector<std::string> Vulkan::queryExtensionsForLayer(
    const char* layerName) {
  uint32_t extensionCount;
  VkResult result = vkEnumerateInstanceExtensionProperties(
      layerName, &extensionCount, nullptr);
  assert(result == VK_SUCCESS);
  std::vector<VkExtensionProperties> extensionProperties(extensionCount);
  result = vkEnumerateInstanceExtensionProperties(layerName, &extensionCount,
                                                  extensionProperties.data());
  assert(result == VK_SUCCESS);

  std::vector<std::string> extensionNames(extensionCount);

  for (size_t i = 0; i < extensionCount; i++) {
    extensionNames[i] = extensionProperties[i].extensionName;
  }

  return extensionNames;
}

void Vulkan::validateLayers() {
  auto layerNames = queryAvailableLayerNames();
  for (auto layerName : m_layerNames) {
    auto it = std::find(layerNames.begin(), layerNames.end(), layerName);
    assert(it != layerNames.end());
  }
}

void Vulkan::validateExtensions() {
  auto extensionNames = queryAvailableExtensions();
  for (auto extensionName : m_extensionNames) {
    auto it =
        std::find(extensionNames.begin(), extensionNames.end(), extensionName);
    assert(it != extensionNames.end());
  }
}

void Vulkan::release() {
  // Remember to destroy Vulkan instance at the very end
  vkDestroyInstance(m_instance, nullptr);
}
