#ifndef XDX_UTILS_HPP
#define XDX_UTILS_HPP

#include <stdio.h>

#define XDX_MSG(msg) printf("[%s:%d] %s\n", __FILE__, __LINE__, msg);

#endif  // XDX_UTILS_HPP
