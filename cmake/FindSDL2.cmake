if( WIN32 )
    find_path( SDL2_INCLUDE_DIRS NAMES SDL.h HINTS
        "${CMAKE_SOURCE_DIR}/contrib/SDL2/include" )
    if( CMAKE_CL_64 )
        find_library( SDL2_LIBRARIES NAMES SDL2 HINTS
            "${CMAKE_SOURCE_DIR}/contrib/SDL2/lib/x64" )
    else()
        find_library( SDL2_LIBRARIES NAMES SDL2 HINTS
            "${CMAKE_SOURCE_DIR}/contrib/SDL2/lib/x86" )
    endif()
else()
    find_path( SDL2_INCLUDE_DIRS NAMES vulkan/vulkan.h HINTS
        "${CMAKE_SOURCE_DIR}/contrib/SDL2/include" )
    find_library( SDL2_LIBRARIES NAMES vulkan HINTS
        "${CMAKE_SOURCE_DIR}/contrib/SDL2/lib" )
endif()

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( 
    SDL2 
    DEFAULT_MSG 
    SDL2_LIBRARIES 
    SDL2_INCLUDE_DIRS 
)

mark_as_advanced(SDL2_INCLUDE_DIRS SDL2_LIBRARIES)
